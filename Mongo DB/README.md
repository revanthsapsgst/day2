# MONGO DB Exercises #

### An admin sends out an email to ‘n’ recipients. Recipient will open his mail box, sees the email content and either or not he replies back. Recipient reply has to be captured back in the same email thread. ###

### Design the collections, schema of the document. Write query for the following workflows: ###
### GET: When admin opens the email thread, all recipients reply should be stacked in same view. ###
### GET: When recipient opens the email thread, his reply is stacked on top of admins email. None of other recipients reply needs to be shown there. ###

### Schema ###

db.email.insertOne({

	"from_mail":"root@psgsoftwaretechnologies.com",
	"to_mail":[
	"vijay@psgsoftwaretechnologies.com",
	"abishekar@psgsoftwaretechnologies.com",
	"anand.sonachalam@psgsoftwaretechnologies.com"
	],
	"message":"Hello, Admin team!",
	"reply":[],
	"date":Date.now()
});

### Post reply ###

db.email.update(

	{"from_mail":"root@psgsoftwaretechnologies.com"},
	{$push:{
	"reply":{
	"from_mail":"abishekar@psgsoftwaretechnologies.com",
	"message":"Hello, root",
	"date":Date.now()
	}
	}}
);

### Get reply of individual user ###

db.email.aggregate([

	{$unwind:"$reply"},
	{$match:{"reply.from_mail":"vijay@psgsoftwaretechnologies.com"}}
]);

### Get reply in order ###

db.email.aggregate([

	{$unwind:"$reply"},
	{$sort:{"reply.date":-1}}
]);