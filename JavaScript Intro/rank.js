var data = require("./Registrations.json");
var course_bcom = [];
var course_bscct = [];

for(i in data){
	if (i.part1LanguageMarks >=35 && i.part2LanguageMarks >=35 && i.sub1Marks >= 35 && i.sub2Marks >= 35 && i.sub3Marks >= 35 && i.sub4Marks >= 35){
		if(i.courseName == "B.Com"){
			course_bcom.push(i);
		} else if(i.courseName == "BSc Computer Technology"){
			if(i.sub1 == "COMPUTER SCIENCE" || i.sub2 == "COMPUTER SCIENCE" || i.sub3 == "COMPUTER SCIENCE" || i.sub4 == "COMPUTER SCIENCE"){
				course_bscct.push(i);
			}
		}
	}
}

function ranking_bcom(a,b) {
	if (a.board == "TNHSE"){
		if (b.board == "TNHSE"){
			
			if(a.cutoff > b.cutoff) return -1;
			
			else if(a.cutoff == b.cutoff){
				
				a_dob = a.dobStr.split("/").reverse();
				b_dob = b.dobStr.split("/").reverse();
				a_diff = new Date(a_dob[0], a_dob[1], a_dob[2]);
				b_diff = new Date(b_dob[0], b_dob[1], b_dob[2]);
				
				if(a_diff.getTime() > b_diff.getTime()) return 1;
				if(a_diff.getTime() == b_diff.getTime()){
					
					if(a.courseRegNo > b.courseRegNo) return -1;
					else return 1;
				}
				else return -1;
			}
			else return 1;
		}
		else return -1;	
	}
	else return 1;
}

function ranking_bscct(a,b) {
	if (a.board != "OTHER"){
		if (b.board != "OTHER"){
			
			if(a.cutoff > b.cutoff) return -1;
			
			else if(a.cutoff == b.cutoff){
				
				a_dob = a.dobStr.split("/").reverse();
				b_dob = b.dobStr.split("/").reverse();
				a_diff = new Date(a_dob[0], a_dob[1], a_dob[2]);
				b_diff = new Date(b_dob[0], b_dob[1], b_dob[2]);
				
				if(a_diff.getTime() > b_diff.getTime()) return 1;
				if(a_diff.getTime() == b_diff.getTime()){
					
					if(a.courseRegNo > b.courseRegNo) return -1;
					else return 1;
				}
				else return -1;
			}
			else return 1;
		}
		else return -1;	
	}
	else return 1;
}

course_bcom.sort(ranking_bcom);
console.log("RANK LIST FOR B.Com");
console.log(course_bcom);

course_bscct.sort(ranking_bscct);
console.log("RANK LIST FOR BSc Computer Technology");
console.log(course_bscct);