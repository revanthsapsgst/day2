// To get the age from given dob
function get_age(data){
    var dob = new Date(data.dob);
    var month_diff = Date.now() - dob.getTime();
    var age_dt = new Date(month_diff);
    var year = age_dt.getUTCFullYear();
    return Math.abs(year - 1970);
}

module.exports.get_age = get_age;