// To get the bmi from given height and weight
function get_bmi(data){
    return data.weight/(data.height*data.height/10000)
}

module.exports.get_bmi = get_bmi;