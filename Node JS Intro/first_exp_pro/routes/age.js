var express = require('express');
var router = express.Router();
var ageservices = require('../services/ageServices');

router.get('/', function(req, res, next) {
  res.send({age: ageservices.get_age(req.body)});
});

module.exports = router;