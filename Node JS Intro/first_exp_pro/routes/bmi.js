var express = require('express');
var router = express.Router();
var bmiservices = require('../services/bmiServices');

router.get('/', function(req, res, next) {
  res.send({bmi: bmiservices.get_bmi(req.body)});
});

module.exports = router;