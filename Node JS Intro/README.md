# Environment Setup #

* pip install nodeenv
* nodeenv nodevenv --node 14.15.4
* .\nodevenv\Scripts\activate
* npm install -g express-generator

# Creating a new project #

* express --view=ejs first_exp_pro

# Running this cloned project #

* cd first_exp_pro
* npm install
* npm start


### Run the following using API calls ###

* Request: http://localhost:3000/age
* Body: {"dob": "12/25/2000"}
* Response: {"age": 20}

* Request: http://localhost:3000/bmi
* Body: {"weight": 54,"height": 173}
* Response: {"bmi": 18.042701059173375}